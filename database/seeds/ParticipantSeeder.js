'use strict'

/*
|--------------------------------------------------------------------------
| ParticipantSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

// class ParticipantSeeder {
//   async run () {
//     await Factory
//         .model('App/Models/Participant')
//         .createMany(1)
//   }
// }

// module.exports = ParticipantSeeder
