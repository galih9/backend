'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Hash = use('Hash')

// Factory.blueprint('App/Models/Room', async () => {   
// })

// Factory.blueprint('App/Models/Room',(faker) => {
//   return {
//     name : faker.username()
//   }
// })

// Factory.blueprint('App/Models/Message', () => {
//     return {
//         room_id : 2,
//         user_id : 2,
//         text_chat : 'privasi'
//     }
// })

// Factory.blueprint('App/Models/Participant', () => {
//     return {
//         room_id : 2,
//         user_id : 2
//     }
// })
