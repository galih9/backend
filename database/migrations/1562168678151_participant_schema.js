'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ParticipantSchema extends Schema {
  up () {
    this.create('participants', (table) => {
      table.increments()
      table.integer('user_id', 10)
        .notNullable()
        .unsigned()
        .references('id')
        .inTable('users')
      table.integer('room_id', 10)
        .notNullable()
        .unsigned()
        .references('id')
        .inTable('rooms')
      table.timestamps()
    })
  }

  down () {
    this.drop('participants')
  }
}

module.exports = ParticipantSchema
