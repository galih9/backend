'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MessageSchema extends Schema {
  up () {
    this.create('messages', (table) => {
      table.increments()
      table.integer('room_id', 10)
        .notNullable()
        .unsigned()
        .references('id')
        .inTable('rooms')
      table.integer('user_id', 10)
        .notNullable()
        .unsigned()
        .references('id')
        .inTable('users')
      table.text('text_chat')
      table.timestamps()
    })
  }

  down () {
    this.drop('messages')
  }
}

module.exports = MessageSchema
