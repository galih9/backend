'use strict' 

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.group(() => {
  Route.post('login', 'LoginController.login')
  Route.post('register', 'LoginController.register')
  Route.get('userid', 'LoginController.getUser').middleware(['auth:jwt'])

  Route.get('users', 'UserController.index').middleware(['auth:jwt'])
  Route.get('user/:id', 'UserController.show').middleware(['auth:jwt'])
  Route.post('users', 'UserController.store').middleware(['auth:jwt'])
  Route.put('user/:id', 'UserController.update').middleware(['auth:jwt'])
  Route.delete('user/:id', 'UserController.delete').middleware(['auth:jwt'])

  Route.get('messages', 'MessageController.index').middleware(['auth:jwt'])
  Route.get('message/:id', 'MessageController.show').middleware(['auth:jwt'])
  Route.post('messages', 'MessageController.store').middleware(['auth:jwt'])
  Route.put('message/:id', 'MessageController.update').middleware(['auth:jwt'])
  Route.delete('message/:id', 'MessageController.delete').middleware(['auth:jwt'])

  Route.get('rooms', 'RoomController.index').middleware(['auth:jwt'])
  Route.get('room/:id', 'RoomController.show').middleware(['auth:jwt'])
  Route.post('rooms', 'RoomController.store').middleware(['auth:jwt'])
  Route.put('room/:id', 'RoomController.update').middleware(['auth:jwt'])
  Route.delete('room/:id', 'RoomController.delete').middleware(['auth:jwt'])

  Route.get('participants', 'ParticipantController.index').middleware(['auth:jwt'])
  Route.get('participant/:id', 'ParticipantController.show').middleware(['auth:jwt'])
  Route.post('participants', 'ParticipantController.store').middleware(['auth:jwt'])
  Route.put('participant/:id', 'ParticipantController.update').middleware(['auth:jwt'])
  Route.delete('participant/:id', 'ParticipantController.delete').middleware(['auth:jwt'])
}).prefix('api/v1')