'use strict'

const Participant = use('App/Models/Participant')

class ParticipantController {
  async index ({ request, response }) {
    try {
      return Participant.all()
    } catch (error) {
      response.send({
        error: true,
        message: error.message
      })
    }
  }

  async show ({ request, response, params }) {
    try {
      return Participant.find(params.id)
    } catch (error) {
      response.send({
        error: true,
        message: error.message
      })
    }
  }

  async store ({ request, response }) {
    try {
      const PartReq = request.only(['user_id', 'room_id'])

      const part = new Participant()
      
      part.name = PartReq.user_id
      part.type = PartReq.room_id
      
      await part.save()
      
      return response.status(201).json(part)
    } catch (error) {
      response.send({
        error: true,
        message: error.message
      })
    }
  }

  async update ({ params, request, response }) {
    const PartReq = request.only(['user_id', 'room_id'])
    const part = await Participant.find(params.id)

    if (!part) {
        return response.status(404).json({message: 'Resource not found'})
    }

    try {
      part.name = PartReq.user_id
      part.type = PartReq.room_id

      await part.save()

      return {
        messsage : "data edited",
        data : part
      }
    } catch (error) {
      response.send({
        error: true,
        message: error.message
      })
    }
  }

  async delete ({ params, request, response }) {
    const part = await Participant.find(params.id)

    if (!part) {
        return response.status(404).json({message: 'Resource not found'})
    }

    try {
      await part.delete()
      return {
          messsage : "data deleted succesfully"
      }
    } catch (error) {
      response.send({
        error: true,
        message: error.message
      })
    }
  }
}
module.exports = ParticipantController
