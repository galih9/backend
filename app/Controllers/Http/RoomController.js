'use strict'

const Room = use('App/Models/Room')

class RoomController {
  async index ({ request, response }) {
    try {
      return Room.all()
    } catch (error) {
      response.send({
        error: true,
        message: error.message
      })
    }
  }

  async show ({ request, response, params }) {
    try {
      return Room.find(params.id)
    } catch (error) {
      response.send({
        error: true,
        message: error.message
      })
    }
  }

  async store ({ request, response, view }) {
    try {
      const RoomReq = request.only(['name', 'type'])

      const room = new Room()
      
      room.name = RoomReq.name
      room.type = RoomReq.type
      
      await room.save()
      
      return response.status(201).json(room)
    } catch (error) {
      response.send({
        error: true,
        message: error.message
      })
    }
  }

  async update ({ params, request, response }) {
    const RoomReq = request.only(['name','type'])
    const room = await Room.find(params.id)

    if (!room) {
        return response.status(404).json({message: 'Resource not found'})
    }

    try {
      room.name = RoomReq.name
      room.type = RoomReq.type

      await room.save()

      return {
        messsage : "data edited",
        data : room
      }
    } catch (error) {
      response.send({
        error: true,
        message: error.message
      })
    }
  }

  async delete ({ params, request, response }) {
    const room = await Room.find(params.id)

    if (!room) {
        return response.status(404).json({message: 'Resource not found'})
    }

    try {
      await room.delete()
      return {
          messsage : "data deleted succesfully"
      }
    } catch (error) {
      response.send({
        error: true,
        message: error.message
      })
    }
  }
}

module.exports = RoomController
