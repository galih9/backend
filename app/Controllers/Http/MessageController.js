'use strict'

const Message = use('App/Models/Message')

class MessageController {
  
  async index ({ request, response, view }) {
    try {
      return await // Message.all()
      Message.query().with('userinfo').fetch()  
      //Message.query().with('roominfo').fetch()  
    } catch (error) {
      response.send({
        error: true,
        message: error.message
      })
    }
  }

  async store ({ request, response }) {
    const MessageReq = request.only(['user_id', 'room_id', 'text_chat'])

    const msg = new Message()
    
    msg.user_id = MessageReq.user_id
    msg.room_id = MessageReq.room_id
    msg.text_chat = MessageReq.text_chat
    
    await msg.save()
    
    return response.status(201).json(msg)
  }

  async show ({ params, request, response }) {
    const message = await Message.find(params.id)
    return {
        messsage : "data colected",
        data : message
    }
  }

  async update ({ params, request, response }) {
    const MessageReq = request.resource('text_chat')
    const message = await Message.find(params.id)
    
    if (!message) {
        return response.status(404).json({data: 'Resource not found'})
    }
    
    message.text_chat = MessageReq.text_chat
    await message.save()

    return {
        messsage : "data edited",
        data : message
    }
  }

  async destroy ({ params, request, response }) {
    const message = await Message.find(params.id)

    if (!message) {
        return response.status(404).json({data: 'Resource not found'})
    }

    await message.delete()
    return {
        messsage : "data deleted succesfully"
    }
  }
}

module.exports = MessageController
