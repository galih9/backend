'use strict'

const User = use('App/Models/User')

class UserController {
    async index ({ request, response }) {
        try {
          return User.all()
        } catch (error) {
          response.send({
            error: true,
            message: error.message
          })
        }
      }
    
      async show ({ request, response, params }) {
        try {
          return User.find(params.id)
        } catch (error) {
          response.send({
            error: true,
            message: error.message
          })
        }
      }
    
      async store ({ request, response, view }) {
        try {
          const UserReq = request.only(['name', 'type'])
    
          const user = new User()
          
          user.name = UserReq.name
          user.type = UserReq.type
          
          await user.save()
          
          return response.status(201).json(user)
        } catch (error) {
          response.send({
            error: true,
            message: error.message
          })
        }
      }
    
      async update ({ params, request, response }) {
        const UserReq = request.only(['name','type'])
        const user = await User.find(params.id)
    
        if (!user) {
            return response.status(404).json({message: 'Resource not found'})
        }
    
        try {
          user.name = UserReq.name
          user.type = UserReq.type
    
          await user.save()
    
          return {
            messsage : "data edited",
            data : user
          }
        } catch (error) {
          response.send({
            error: true,
            message: error.message
          })
        }
      }
    
      async delete ({ params, request, response }) {
        const user = await User.find(params.id)
    
        if (!user) {
            return response.status(404).json({message: 'Resource not found'})
        }
    
        try {
          await user.delete()
          return {
              messsage : "data deleted succesfully"
          }
        } catch (error) {
          response.send({
            error: true,
            message: error.message
          })
        }
    }
}

module.exports = UserController
