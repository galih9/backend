'use strict'

const User = use('App/Models/User')
const Hash = use('Hash')

class UserController {
    async login ({ request, auth }) {
        const { email, password } = request.all()
        await auth .attempt(email, password)
        
        const user = await User.findBy('email', email)
        
        const token = await auth.generate(user)
    
        return auth.withRefreshToken().attempt(email, password)
    }

    async show ({ auth, params }) {
        const user = await User.find(params.id)
        return {
        messsage : "data colected",
        data : user
    }
    }
    
    async register ({ auth, params, request, response }) {
        const UserReq = request.only(['username', 'email', 'password'])
        const users = new User()

        try {
            users.username = UserReq.username
            users.email = UserReq.email
            users.password = UserReq.password
            
            await users.save()
            return response.status(201).json(users)
        }  catch (error) {
            response.send({
              error: true,
              message: error.message
            })
        }
        
    }

    async getUser ({ request,response, auth }){
        try {
            return response.send(auth.current.user)
        } catch (error) {
            response.send({
                error: true,
                message: error.message
            })
        }
        
    }
}

module.exports = UserController
