'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Message extends Model {

    userinfo (){
        return this.hasMany('App/Models/User', 'user_id','id')
    }
    
    roominfo(){
        return this.hasMany('App/Models/Room','room_id','id')
    }
}

module.exports = Message
